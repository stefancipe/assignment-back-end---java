# Assignment Object oriented - Java

Code and description for Java OOP assignment for Prime Holding internship. The application is started by executing the main method from Main.java, but there's also a <b><a href="https://trinket.io/java/25e4de2efc">trinket</a></b> with this code. So, the fastest way to start the application is to simply click on the trinket link and have the code executed in your browser right away.


<h1>Code explained</h1>
<h2>Main.java</h2>
Application entry point. Contains main function with the required data input from the examples. 
<h2>DiscountCard.java</h2>
Abstract class containing most of the functionalities that the Card has. Attributes of this class are genId, id, owner, discountRate, previousTurnover and currentTurnover:
<ul>
    <li>{+genId+} is a static int attribute used for generating unique ids.</li>
    <li>{+id+} is a final int attribute used to differentiate different cards.</li>
    <li>{+owner+} is a String attribute containing owner's name.</li>
    <li>{+discountRate+} is a double attribute for storing discount rate.</li>
    <li>{+previousTurnover+} is a double attribute that has </li>
    <li>{+currentTurnover+}</li>
</ul>
Aside from the constructors, getters and setters, there are monthlyUpdate, confirmPurchase and calculateDiscount methods, and abstract setDiscountRate method. The method {+monthlyUpdate()+} is intended to be run once a month, set currentTurnover to 0, change the previousTurnover to the value that was currentTurnover up until now, and calculate new discount rate for this month using setDiscountRate method. The method {+confirmPurchase()+} updates currentTurnover for the value of a purchase. The method {+calculateDiscount()+} returns the amount of discount for purchase calculated using formula valueOfPurchase * discountRate.
Implementation of the method {+setDiscountRate()+} is to be determined by the child classes. 
<h2>BronzeCard.java</h2>
BronzeCard class extends DiscountCard class and implements {+setDiscountRate()+} method by setting the discountRate to 0 if the previousTurnover was less than $100, 0.01 if it's between $100 and $300 or to 0.025 if it's greater than $300.
<h2>SilverCard.java</h2>
SilverCard class also extends DiscountCard, and {+setDiscountRate()+} is implemented here to set discountRate to 0.02 if the previousTurnover was less than or equal to $300, or to 0.035 if it was greater than $300. 
<h2>GoldCard.java</h2>
GoldCard class extends DiscountCard as well, with {+setDiscountRate()+} being implemented to scale the discountRate with the previousTurnover, having an increase in discountRate by 0.01 for each $100 spent, but also capping at 0.1
<h2>PayDesk.java</h2>
PayDesk class has a single static method {+scanCard()+} that receives an instance of a DiscountCard and a value of a purchase. It prints out the purchase value, the discount rate, the amount of discount for the purchase and the total amount to pay. After that, the confirmPurchase method is called from the DiscountCard instance for the total. This part could be updated to prompt for input from user to confirm the purchase after displaying the applied discount.
