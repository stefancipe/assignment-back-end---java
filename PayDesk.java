public class PayDesk {

	public static void scanCard(DiscountCard discountCard, double valueOfPurchase) {
		System.out.printf("Purchase value: $%.2f\n",valueOfPurchase);
		System.out.printf("Discount rate: %.1f",discountCard.getDiscountRate()*100);
		System.out.println("%");
		System.out.printf("Discount: $%.2f\n",discountCard.calculateDiscount(valueOfPurchase));
		System.out.printf("Total: $%.2f\n\n",valueOfPurchase - discountCard.calculateDiscount(valueOfPurchase));
		
		discountCard.confirmPurchase(valueOfPurchase - discountCard.calculateDiscount(valueOfPurchase));
	}
}
