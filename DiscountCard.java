public abstract class DiscountCard {
	private static int genId;
	private final int id;
	private String owner;
	protected double discountRate;
	private double previousTurnover;
	private double currentTurnover;
	
	public DiscountCard(String owner, double discountRate) {
		this.id = genId++; //id is set when the object is constructed and cannot be changed onward
		this.owner = owner;
		this.discountRate = discountRate;
		this.previousTurnover = 0.0;
		this.currentTurnover = 0.0;
	}
	
	public DiscountCard(String owner, double previousTurnover, double currentTurnover) {
		this.id = genId++;
		this.owner = owner;
		this.previousTurnover = previousTurnover;
		this.currentTurnover = currentTurnover;
	}

	public int getId() {
		return id;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public double getPreviousTurnover() {
		return previousTurnover;
	}

	public void setPreviousTurnover(double previousTurnover) {
		this.previousTurnover = previousTurnover;
	}

	public double getCurrentTurnover() {
		return currentTurnover;
	}

	public void setCurrentTurnover(double currentTurnover) {
		this.currentTurnover = currentTurnover;
	}
	
	public void monthlyUpdate() {
		this.previousTurnover = this.currentTurnover;
		this.currentTurnover = 0.0;
		this.setDiscountRate(); //apart from assigning new values to current and previous turnover, we also 
		//calculate new discount rate for the month
	}
	
	public abstract void setDiscountRate();
	
	public double calculateDiscount(double valueOfPurchase) {
		return valueOfPurchase*this.discountRate;
	}
	
	public void confirmPurchase(double valueOfPurchase) {
		this.currentTurnover += valueOfPurchase;
	}
}
