public class SilverCard extends DiscountCard{
	public SilverCard(String owner) {
		super(owner, 0.02);
	}

	public SilverCard(String owner, double previousTurnover) {
		super(owner, previousTurnover, 0.0);
		setDiscountRate();
	}
	
	public SilverCard(String owner, double previousTurnover, double currentTurnover) {
		super(owner, previousTurnover, currentTurnover);
		setDiscountRate();
	}
	
	@Override
	public void setDiscountRate() {
		if (this.getPreviousTurnover()<=300.00)
			this.discountRate = 0.02;
		else this.discountRate = 0.035;
	}
}
