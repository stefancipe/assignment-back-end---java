public class BronzeCard extends DiscountCard{

	public BronzeCard(String owner) {
		super(owner, 0.0);
	}

	public BronzeCard(String owner, double previousTurnover) {
		super(owner,previousTurnover,0.0);
		setDiscountRate();
	}
	
	public BronzeCard(String owner, double previousTurnover, double currentTurnover) {
		super(owner,previousTurnover,currentTurnover);
		setDiscountRate();
	}
	
	@Override
	public void setDiscountRate() {
		if(this.getPreviousTurnover() < 100.00)
			this.discountRate = 0;
		else if (this.getPreviousTurnover()<=300.00)
			this.discountRate = 0.01;
		else this.discountRate = 0.025;
	}

}
