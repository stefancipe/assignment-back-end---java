public class Main {

	public static void main(String[] args) {
		DiscountCard dc1 = new BronzeCard("Nick");
		PayDesk.scanCard(dc1, 150);
		DiscountCard dc2 = new SilverCard("Antonio",600.00);
		PayDesk.scanCard(dc2, 850);
		DiscountCard dc3 = new GoldCard("Mark",1500.00);
		PayDesk.scanCard(dc3, 1300);	
	}	

}
