public class GoldCard extends DiscountCard {
	public GoldCard(String owner) {
		super(owner, 0.02);
	}

	public GoldCard(String owner, double previousTurnover) {
		super(owner,previousTurnover,0.0);
		setDiscountRate();
	}
	
	public GoldCard(String owner, double previousTurnover, double currentTurnover) {
		super(owner,previousTurnover,currentTurnover);
		setDiscountRate();
	}
	
	@Override
	public void setDiscountRate() {
		double scaledValue = ((int)this.getPreviousTurnover()/100)/100.00;
		if( scaledValue < 0.08) {
			this.discountRate = 0.02 + scaledValue;
		}
		else
			this.discountRate = 0.10;
	}
}
